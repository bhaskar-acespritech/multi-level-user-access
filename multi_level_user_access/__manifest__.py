# -*- coding: utf-8 -*-
{
    'name': 'Multi Level User Access',
    'version': '13.0.1.0.0',
    'summary': 'Multi Level User Access',
    'author': 'Acespritech Pvt. Ltd.',
    'description': 'Multi Level User Access',
    'website': 'www.acespritech.com',
    'depends': ['base', 'hr', 'crm', 'project'],
    'category': 'hr',
    'data': [
        'security/security_data.xml',
        'security/ir.model.access.csv',
        'views/menu.xml',
    ],
    'qweb': [
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'post_init_hook': 'post_init',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
