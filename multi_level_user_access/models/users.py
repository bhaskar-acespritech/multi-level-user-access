# -*- coding: utf-8 -*-
from odoo import models


class ResUsers(models.Model):
    _inherit = 'res.users'

    def get_employee(self):
        employee = self.env['hr.employee'].sudo().search([('user_id', '=', self.id)])
        employees_to_recalculate = employee.ids
        current_employees = employee
        while current_employees:
            # For the current employee, find their all of their child_ids
            current_employees_2 = current_employees.mapped('child_ids').ids
            res = []
            [res.append(x) for x in current_employees_2 if x not in res]
            # current_moves_2 -= moves_to_recalculate
            # moves_to_recalculate |= current_moves_2

            employees_to_recalculate.extend(res)
            current_employees = self.env['hr.employee'].sudo().browse(res)

        employee_user_ids = self.env['hr.employee'].sudo().browse(employees_to_recalculate).mapped('user_id').ids
        return employee_user_ids

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
