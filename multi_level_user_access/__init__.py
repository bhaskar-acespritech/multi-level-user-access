# -*- coding: utf-8 -*-
from . import models
from odoo.api import Environment, SUPERUSER_ID


def post_init(cr, registry):
    env = Environment(cr, SUPERUSER_ID, {})
    record_rule = env.ref('project.task_visibility_rule')
    if record_rule:
        record_rule.active = False
    rule = env.ref('project.project_public_members_rule')
    if rule:
        rule.active = False

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
